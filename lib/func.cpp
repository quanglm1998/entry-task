#include <vector>
#include <string>

#include <glog/logging.h>

extern "C" {

std::vector<std::string> split(const std::string &str, char seperator) {
  std::vector<std::string> result;
  std::string current_string = "";
  for (auto &c : str) {
    if (c == seperator) {
      result.push_back(current_string);
      current_string = "";
    } else {
      current_string += c;
    }
  }
  result.push_back(current_string);
  return result;
}

std::string sum(const std::string &args) {
  auto arg_vec = split(args, ',');
  int res = 0;
  for (auto &arg : arg_vec) {
    res += std::stoi(arg);
  }
  return std::to_string(res);
}

std::string product(const std::string &args) {
  auto arg_vec = split(args, ',');
  int res = 1;
  for (auto &arg : arg_vec) {
    res *= std::stoi(arg);
  }
  return std::to_string(res);
}

}