#include "src/main/cpp/socket_client.h"
#include <folly/io/async/AsyncSocketException.h>

namespace xmpl {

namespace socket { 

void SocketClient::Connect(const std::string &host, int port) {
  auto address = folly::SocketAddress(host, port);

  connect_callback_ = std::make_shared<ConnectCallback>();
  socket_ = folly::AsyncSocket::newSocket(event_base_);
  socket_->connect(connect_callback_.get(), address);

  connect_callback_->setSuccessCallback([this]() {
    Process();
  });
  connect_callback_->setErrorCallback([this](const folly::AsyncSocketException &ex) {
    HandleError(ex);
  });
}

/**
 * GetWriteContent() -> write content to socket stream
 *
 */
void SocketClient::Process() noexcept {
  write_callback_ = std::make_shared<WriteCallback>();
  read_callback_ = std::make_shared<ReadCallback>();

  write_callback_->setSuccessCallback([this, read_callback = read_callback_]() {
  });
  write_callback_->setErrorCallback([this](size_t, const folly::AsyncSocketException &ex) {
    HandleErrorWriteResponse(ex);
  });

  read_callback_->setReadCallback([this](const char *buf, size_t len) {
    HandleResponse(buf, len);
  });
  read_callback_->setErrorCallback([this](const folly::AsyncSocketException &ex) {
    HandleErrorReadResponse(ex);
  });
  socket_->setReadCB(read_callback_.get());

  GetWriteContent().thenValue([this](std::string content) {
    socket_->write(write_callback_.get(), content.c_str(), content.size());
  });
}

}  // namespace socket

}  // namespace dslib