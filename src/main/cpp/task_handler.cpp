#include <map>
#include <unistd.h>
#include <dlfcn.h> 
#include <functional>
#include <cstdlib>

#include <glog/logging.h>
#include <libconfig.h++>

#include "src/main/cpp/task_handler.h"

namespace xmpl {

void TaskHandler::LoadTask(const std::string &path, const std::string &name) {
  void* lib;
  lib = dlopen(path.c_str(), RTLD_NOW);
  if (lib == nullptr) {
    LOG(INFO) << dlerror();
  }

  auto df = reinterpret_cast<fn_def>(dlsym(lib, name.c_str()));

  if (df) { 
    string_to_task[name] = df;
  } else {
    LOG(INFO) << dlerror();
  }
}

std::string TaskHandler::RunTask(const std::string &name, const std::string &args) {
  if (string_to_task.find(name) == string_to_task.end()) {
    LOG(INFO) << "Task with name " << name << " has not been loaded!";
    return "";
  }
  auto task = string_to_task[name];
  return task(args);
}

void TaskHandler::LoadConfig(const std::string &config_path) {
  libconfig::Config cfg;

  // Read the file. If there is an error, report it and exit.
  try {
    cfg.readFile(config_path);
  } catch(const libconfig::FileIOException &fioex) {
    LOG(INFO) << "I/O error while reading file";
  } catch(const libconfig::ParseException &pex) {
    LOG(INFO) << "Parse error at " << pex.getFile() << ":" << pex.getLine()
              << " - " << pex.getError();
  }

  const libconfig::Setting& root = cfg.getRoot();
  try {
    auto &tasks = root["task"];
    for(int i = 0; i < tasks.getLength(); ++i) {
      auto &task = tasks[i];
      std::string name;
      std::string lib_path;

      task.lookupValue("name", name);
      task.lookupValue("libpath", lib_path);
      LoadTask(lib_path, name);
    }
  } catch(const libconfig::SettingNotFoundException &nfex) {
    // Ignore.
  }
}

} // namespace xmpl