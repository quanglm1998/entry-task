#pragma once

#include <map>
#include <unistd.h>
#include <dlfcn.h> 
#include <functional>

namespace xmpl {

typedef std::string (*fn_def)(std::string);

class TaskHandler {
 public:
  static TaskHandler& GetInstance() {
    static TaskHandler instance; // Guaranteed to be destroyed, instantiated on first use.
    return instance;
  }

  void LoadConfig(const std::string &config_path);

  void LoadTask(const std::string &path, const std::string &name);

  std::string RunTask(const std::string &name, const std::string &args);

 private:
  TaskHandler() {}
  TaskHandler(TaskHandler const&);
  void operator=(TaskHandler const&);

  std::map<std::string, fn_def> string_to_task;
};

} // namespace xmpl
