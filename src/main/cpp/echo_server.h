#pragma once

#include <string>
#include <memory>
#include <thread>

#include <folly/SocketAddress.h>
#include <folly/io/async/AsyncSocket.h>
#include <folly/io/async/AsyncSocketException.h>

#include "protobuf/task.pb.h"
#include "src/main/cpp/socket_server.h"
#include "src/main/cpp/task_handler.h"

namespace xmpl {

namespace socket {

class EchoServer : public SocketServer {
 protected:
  folly::Future<std::string> HandleRequest(const folly::SocketAddress &,
                                           const char *buffer,
                                           size_t len) noexcept override {
    std::string buffer_string(buffer);

    xmpl::Task task;
    if (!task.ParseFromString(buffer_string)) {
      LOG(INFO) << "Cannot parse task!";
    }
    
    auto task_name = task.name();
    std::string args;
    folly::join(",", task.args().begin(), task.args().end(), args);

    auto content = TaskHandler::GetInstance().RunTask(task_name, args);

    Result result;
    result.set_result(content);

    return result.SerializeAsString();
  }

  void HandleError(const std::exception &ex) noexcept override {
    LOG(INFO) << "Connection error " << ex.what();
  }

 private:
  std::vector<folly::AsyncSocket::UniquePtr> sockets_;
};

} // namespace socket

} // namespace xmpl
