#include <iostream>
#include <memory>
#include <thread>

#include <folly/SocketAddress.h>
#include <folly/io/async/AsyncSocket.h>
#include <folly/io/async/AsyncSocketException.h>
#include <folly/String.h>

#include "protobuf/task.pb.h"
#include "src/main/cpp/socket_client.h"

namespace xmpl {

namespace socket {

class MyClient : public SocketClient {
 public:
  explicit MyClient(folly::EventBase *evb) : SocketClient(evb) {}

  folly::Future<std::string> GetWriteContent() noexcept override {
    xmpl::Task task;
    task.set_name(task_);
    std::vector<std::string> args;
    folly::split(",", args_, args);
    for (std::string &arg : args) {
      task.add_args(arg);
    }
    return task.SerializeAsString();
  }

  void HandleResponse(const char *buf, size_t len) noexcept override {
    std::string buffer_string(buf);
    Result result;
    if (!result.ParseFromString(buffer_string)) {
      LOG(INFO) << "Cannot parse task!";
      return;
    }

    LOG(INFO) << "RESULT = " << result.result();
  }

  void Init(std::string task, std::string args) {
    task_ = task;
    args_ = args;
  }

 private:
  std::string task_;
  std::string args_;
};

} // namespace socket

} // namespace xmpl
