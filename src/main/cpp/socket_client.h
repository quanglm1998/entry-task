#pragma once

#include <folly/SocketAddress.h>
#include <folly/futures/Future.h>
#include <folly/io/async/AsyncSocket.h>
#include <folly/io/async/AsyncSocketException.h>
#include <folly/io/async/EventBase.h>
#include <boost/core/noncopyable.hpp>

#include "src/main/cpp/io_callbacks.h"

namespace xmpl {

namespace socket {

class SocketClient : public boost::noncopyable {
 public:
  SocketClient(folly::EventBase *evb) : event_base_(evb) { }

  SocketClient(SocketClient &&other) {
    event_base_ = other.event_base_;
    socket_ = std::move(other.socket_);
    connect_callback_ = std::move(connect_callback_);
  }

  virtual ~SocketClient() {
    if (socket_) {
      socket_->close();
    }
  }

  void Connect(const std::string &host, int port);

  virtual void Process() noexcept;

  virtual folly::Future<std::string> GetWriteContent() noexcept = 0;

  virtual void HandleError(const folly::AsyncSocketException &ex) noexcept { }
  virtual void HandleResponse(const char *buf, size_t len) noexcept = 0;
  virtual void HandleErrorReadResponse(const std::exception &ex) noexcept { }
  virtual void HandleErrorWriteResponse(const std::exception &ex) noexcept { }

 protected:
  folly::AsyncSocket::UniquePtr socket_;
  folly::EventBase *event_base_;
  std::shared_ptr<ConnectCallback> connect_callback_;
  std::shared_ptr<WriteCallback> write_callback_;
  std::shared_ptr<ReadCallback> read_callback_;
};

}  // namespace socket

}  // namespace dslib