#include <iostream>
#include <fstream>
#include <string>
#include "protobuf/task.pb.h"

xmpl::Task InputTask() {
  xmpl::Task task;
  std::cout << "Task name: ";
  std::string name;
  std::cin >> name;
  task.set_name(name);
  std::cout << "Num args: ";
  int num_args;
  std::cin >> num_args;
  while (num_args--) {
    std::string arg;
    std::cout << "arg=";
    std::cin >> arg;
    task.add_args(arg);
  }
  return task;
}

// Main function:  Reads the entire address book from a file,
//   adds one person based on user input, then writes it back out to the same
//   file.
int main(int argc, char* argv[]) {
  // Verify that the version of the library that we linked against is
  // compatible with the version of the headers we compiled against.
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  if (argc != 2) {
    std::cerr << "Usage:  " << argv[0] << " ADDRESS_BOOK_FILE" << std::endl;
    return -1;
  }

  // xmpl::Task task = InputTask();

  xmpl::Task task;

  {
    // Read the existing address book.
    std::fstream input(argv[1], std::ios::in | std::ios::binary);
    if (!input) {
      std::cout << argv[1] << ": File not found.  Creating a new file." << std::endl;
    } else if (!task.ParseFromIstream(&input)) {
      std::cerr << "Failed to parse address book." << std::endl;
      return -1;
    }
  }  

  std::cerr << task.name() << std::endl;

  // // Add an address.
  // PromptForAddress(address_book.add_people());

  // {
  //   // Write the new address book back to disk.
  //   std::fstream output(argv[1], std::ios::out | std::ios::trunc | std::ios::binary);
  //   system("pwd");
  //   if (!task.SerializeToOstream(&output)) {
  //     std::cerr << "Failed to write task book." << std::endl;
  //     return -1;
  //   } else {
  //     std::cerr << "Done! " << argv[1] << std::endl;
  //   }
  // }

  // Optional:  Delete all global objects allocated by libprotobuf.
  google::protobuf::ShutdownProtobufLibrary();

  return 0;
}



//


#include <iostream>
#include <fstream>

#include <folly/experimental/ProgramOptions.h>

using namespace boost;
using po = boost::program_options;
using namespace std;

int main(int ac, char* av[]) {
try {
        int opt;
        string config_file;
    
        // Declare a group of options that will be 
        // allowed only on command line
        po::options_description generic("Generic options");
        generic.add_options()
            ("version,v", "print version string")
            ("help", "produce help message")
            ("config,c", po::value<string>(&config_file)->default_value("multiple_sources.cfg"),
                  "name of a file of a configuration.")
            ;
    
        // Declare a group of options that will be 
        // allowed both on command line and in
        // config file
        po::options_description config("Configuration");
        config.add_options()
            ("optimization", po::value<int>(&opt)->default_value(10), 
                  "optimization level")
            ("include-path,I", 
                 po::value< vector<string> >()->composing(), 
                 "include path")
            ;

        // Hidden options, will be allowed both on command line and
        // in config file, but will not be shown to the user.
        po::options_description hidden("Hidden options");
        hidden.add_options()
            ("input-file", po::value< vector<string> >(), "input file")
            ;

        
        po::options_description cmdline_options;
        cmdline_options.add(generic).add(config).add(hidden);

        po::options_description config_file_options;
        config_file_options.add(config).add(hidden);

        po::options_description visible("Allowed options");
        visible.add(generic).add(config);
        
        po::positional_options_description p;
        p.add("input-file", -1);
        
        po::variables_map vm;
        store(po::command_line_parser(ac, av).
              options(cmdline_options).positional(p).run(), vm);
        notify(vm);
        
        ifstream ifs(config_file.c_str());
        if (!ifs)
        {
            cout << "can not open config file: " << config_file << "\n";
            return 0;
        }
        else
        {
            store(parse_config_file(ifs, config_file_options), vm);
            notify(vm);
        }
    
        if (vm.count("help")) {
            cout << visible << "\n";
            return 0;
        }

        if (vm.count("version")) {
            cout << "Multiple sources example, version 1.0\n";
            return 0;
        }

        if (vm.count("include-path"))
        {
            cout << "Include paths are: " 
                 << vm["include-path"].as< vector<string> >() << "\n";
        }

        if (vm.count("input-file"))
        {
            cout << "Input files are: " 
                 << vm["input-file"].as< vector<string> >() << "\n";
        }

        cout << "Optimization level is " << opt << "\n";                
    }
    catch(exception& e)
    {
        cout << e.what() << "\n";
        return 1;
    }    
  return 0;
}
