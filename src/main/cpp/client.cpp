#include <iostream>

#include <folly/futures/Future.h>
#include <folly/executors/ThreadedExecutor.h>
#include <folly/executors/IOThreadPoolExecutor.h>

#include "src/main/cpp/my_client.h"

DEFINE_string(host, "127.0.0.1", "host to send request to");
DEFINE_int32(port, 3333, "Port");
DEFINE_string(task, "", "task");
DEFINE_string(args, "", "args");

int main(int argc, char *argv[]) {
  FLAGS_logtostderr = true;
  // Initialize Google's logging library.
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  if (FLAGS_task.empty()) {
    gflags::ShowUsageWithFlags(argv[0]);
    LOG(INFO) << "-task is required!";
    exit(1);
  }

  if (FLAGS_args.empty()) {
    gflags::ShowUsageWithFlags(argv[0]);
    LOG(INFO) << "-args is required!";
    exit(1);
  }

  folly::IOThreadPoolExecutor executor(1);
  auto event_base = executor.getEventBase();
  auto client = std::make_shared<xmpl::socket::MyClient>(event_base);
  event_base->runInEventBaseThread(folly::Func([event_base, client]() mutable {
    client->Init(FLAGS_task, FLAGS_args);
    client->Connect(FLAGS_host, FLAGS_port);
  }));
  // Put main thread to sleep forever
  folly::Promise<int>().getFuture().wait();
  return 0;
}
