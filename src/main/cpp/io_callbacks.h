#pragma once

#include <folly/io/async/AsyncSocket.h>

namespace xmpl {

namespace socket {

enum CallbackState { WAITING, SUCCEEDED, FAILED };

class ConnectCallback : public folly::AsyncSocket::ConnectCallback {
 public:
  using ErrorCallback  = folly::Function<void(const folly::AsyncSocketException&)>;
  using SuccessCallback = folly::Function<void()>;

  ConnectCallback() : state_(WAITING) { }

  void connectSuccess() noexcept override {
    state_ = SUCCEEDED;
    LOG(INFO) << "SUCCEEDED";
    if (success_callback_) {
      success_callback_();
    }
  }

  void connectErr(const folly::AsyncSocketException& ex) noexcept override {
    state_ = FAILED;
    LOG(INFO) << "FAILED" << ex.what();
    if (error_callback_) {
      error_callback_(ex);
    }
  }

  void setSuccessCallback(SuccessCallback cb) {
    success_callback_ = std::move(cb);
  }

  void setErrorCallback(ErrorCallback cb) {
    error_callback_ = std::move(cb);
  }

  CallbackState state() {
    return state_;
  }

 private:
  CallbackState state_;
  SuccessCallback success_callback_;
  ErrorCallback error_callback_;
};

class ReadCallback : public folly::AsyncReader::ReadCallback {
 public:
  static constexpr int DEFAULT_BUFFER_LEN = 8192;

  using SocketEOFCallback = folly::Func;
  using SocketErrorCallback = folly::Function<void(const folly::AsyncSocketException &)>;
  using SocketReadCallback = folly::Function<void(const char *buffer, size_t len)>;

  ReadCallback(size_t max_buffer_size = DEFAULT_BUFFER_LEN)
      : state_(WAITING), buffer_sz_(max_buffer_size) { }
  ~ReadCallback() {
    delete[] buffer_;
  }

  void getReadBuffer(void **bufReturn, size_t *len_return) override {
    if (!buffer_) {
      buffer_ = new char[buffer_sz_]();
    }
    *bufReturn = buffer_;
    *len_return = buffer_sz_;
  }

  void readDataAvailable(size_t len) noexcept override {
    read_callback_(buffer_, len);
  }

  void readEOF() noexcept {
    state_ = SUCCEEDED;
    if (eof_callback_) eof_callback_();
  }

  void readErr(const folly::AsyncSocketException &ex) noexcept {
    state_ = FAILED;
    if (err_callback_) err_callback_(ex);
  }

  void setReadCallback(SocketReadCallback cb) {
    read_callback_ = std::move(cb);
  }

  void setEOFCallback(SocketEOFCallback cb) {
    eof_callback_ = std::move(cb);
  }

  void setErrorCallback(SocketErrorCallback cb) {
    err_callback_ = std::move(cb);
  }

  CallbackState state() {
    return state_;
  }

 private:
  CallbackState state_;
  size_t buffer_sz_;
  char *buffer_ = nullptr;
  SocketReadCallback read_callback_;
  SocketEOFCallback eof_callback_;
  SocketErrorCallback err_callback_;
};

class WriteCallback : public folly::AsyncWriter::WriteCallback {
 public:
  using ErrorCallback  = folly::Function<void(size_t, const folly::AsyncSocketException&)>;
  using SuccessCallback = folly::Function<void()>;

  WriteCallback() : state_(WAITING) { }

  void writeSuccess() noexcept {
    state_ = SUCCEEDED;
    if (success_callback_) success_callback_();
  }

  void writeErr(size_t bytesWritten, const folly::AsyncSocketException &ex) noexcept {
    state_ = FAILED;
    if (error_callback_) error_callback_(bytesWritten, ex);
  }

  void setSuccessCallback(SuccessCallback cb) {
    success_callback_ = std::move(cb);
  }

  void setErrorCallback(ErrorCallback cb) {
    error_callback_ = std::move(cb);
  }

  CallbackState state() {
    return state_;
  }

 private:
  CallbackState state_;
  SuccessCallback success_callback_;
  ErrorCallback error_callback_;
};

}  // namespace socket

}  // namespace xmpl
